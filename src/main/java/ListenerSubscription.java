import java.util.Map;

public interface ListenerSubscription {
    void onMessage(Map<String, String> headers, String body);
}