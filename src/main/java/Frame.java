
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class Frame {
    private final String              command;
    private final Map<String, String> headers;
    private final String              body;

    /**
     * Constructor of a Frame object. All parameters of a frame can be instantiate
     */
    public Frame(String command, Map<String, String> headers, String body) {
        this.command = command;
        this.headers = headers != null ? headers : new HashMap<String, String>();
        this.body = body != null ? body : "";
    }

    public String getCommand() {
        return command;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getBody() {
        return body;
    }

    /**
     * Transform a frame object into a String. This method is copied on the objective C one, in the MMPReactiveStompClient
     * library
     *
     * @return a frame object convert in a String
     */
    public String marshall() {
        StringBuilder sb = new StringBuilder(this.command)
                .append(Byte.LF);
        for (Map.Entry<String, String> entry : this.headers.entrySet()) {
            sb.append(entry.getKey())
                    .append(':')
                    .append(entry.getValue())
                    .append(Byte.LF);
        }
        sb.append(Byte.LF)
                .append(this.body)
                .append(Byte.NULL);

        return sb.toString();
    }

    /**
     * Create a frame from a received message. This method is copied on the objective C one, in the MMPReactiveStompClient
     * library
     *
     * @param data a part of the message received from network, which represented a frame
     * @return An object frame
     */
    public static Frame unmarshallSingle(String data) {
        List<String> contents = new ArrayList<>(Arrays.asList(data.split(String.valueOf(Byte.LF))));

        while (contents.size() > 0 && contents.get(0).equals("")) {
            contents.remove(0);
        }

        if (contents.size() == 0) {
            return null;
        }
        String command = contents.get(0);
        Map<String, String> headers = new HashMap<>();
        StringBuilder body = new StringBuilder();

        contents.remove(0);
        boolean inBody = false;
        for (String line : contents) {
            if (inBody) {
                for (int i = 0; i < line.length(); i++) {
                    char c = line.charAt(i);
                    if (c != Byte.NULL)
                        body.append(c);
                }
            } else if (line.isEmpty()) {
                inBody = true;
            } else {
                String[] header = line.split(":");
                if (header.length == 2) {
                    headers.put(header[0], stompUnescape(header[1]));
                } else {
                    log.warn("Invalid header: {}", line);
                }
            }
        }
        return new Frame(command, headers, body.toString());
    }

    private static String stompUnescape(String s) {
        if (s.indexOf('\\') == -1) {
            return s;
        }
        StringBuilder sb = new StringBuilder();
        boolean escapedChar = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (escapedChar) {
                switch (c) {
                    case 'r':
                        c = '\r';
                        break;

                    case 'n':
                        c = '\n';
                        break;

                    case 'c':
                        c = ':';
                        break;

                    default:
                        break;
                }
                escapedChar = false;
            } else if (c == '\\') {
                escapedChar = true;
                continue;
            }
            sb.append(c);
        }
        return sb.toString();
    }

//    No need this method, a single frame will be always be send because body of the message will never be excessive
//    /**
//     * Transform a message received from server in a Set of objects, named frame, manageable by java
//     * 
//     * @param datas
//     *        message received from network
//     * @return
//     *        a Set of Frame
//     */
//    public static Set<Frame> unmarshall(String datas){
//      String data;
//      String[] ref = datas.split(Byte.NULL + Byte.LF + "*");//NEED TO VERIFY THIS PARAMETER
//      Set<Frame> results = new HashSet<Frame>();
//      
//      for (int i = 0, len = ref.length; i < len; i++) {
//            data = ref[i];
//            
//            if ((data != null ? data.length() : 0) > 0){
//              results.add(unmarshallSingle(data));
//            }
//        }         
//      return results;
//    }

    private static class Byte {
        public static final char LF   = '\n';
        public static final char NULL = '\0';
    }
}