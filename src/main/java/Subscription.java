import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@Accessors(chain = true)
public class Subscription {
    private final Map<String, String> extraHeaders = new HashMap<>();

    private final String destination;

    private String               id;
    private ListenerSubscription listener;

    public Subscription addExtraHeader(String key, String value) {
        extraHeaders.put(key, value);
        return this;
    }
}
