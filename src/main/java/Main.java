import java.io.IOException;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        Stomp stomp = new Stomp("ws://localhost:8080/muxipay/monitor", null, new ListenerWSNetwork() {
            @Override
            public void onState(int state) {
                System.err.printf("State: %d%n", state);
            }
        });
        stomp.connect();
        Subscription subscription = new Subscription("/topic/MuxiPay.M11111111000191.L1.BN")
                .addExtraHeader("activemq.retroactive", "true")
                .setListener(new ListenerSubscription() {
                    @Override
                    public void onMessage(Map<String, String> headers, String body) {
                        System.err.printf("%n*** NEW MESSAGE ***%n%n");
                        for (Map.Entry<String, String> entry : headers.entrySet()) {
                            System.err.printf("%s:%s%n", entry.getKey(), entry.getValue());
                        }
                        System.err.println();
                        System.err.println("body = " + body);
                    }
                });
        stomp.subscribe(subscription);

        System.in.read();

        stomp.unsubscribe(subscription.getId());
        stomp.disconnect();
    }
}
